<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormRegistroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['label' => 'Nombre'])
            ->add('apellidos', TextType::class, ['label' => 'Apellidos'])
            ->add('username', TextType::class, ['label' => 'Nick'])
            ->add('password', PasswordType::class, ['label' => 'Password'])
            ->add('correo', EmailType::class, ['label' => 'Email'])

       /*     ->add('idioma', TextType::class, ['label' => 'Idioma']) */
         /*   ->add('avatar', FileType::class,
                    ['label' => 'Avatar JPG/PNG',
                      'mapped' => false,
                        'required' => false])*/
            ->add('provincia', TextType::class, ['label' => 'Provincia'])
            ->add('guardar', SubmitType::class, array('label' => 'Guardar'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}


