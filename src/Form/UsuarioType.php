<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellidos')
            ->add('username')
            ->add('password')
            ->add('correo')
         /*   ->add('fechaAlta')
            ->add('role')
            ->add('idioma')*/
         ->add('avatar', FileType::class,
             ['label' => 'Avatar JPG/PNG',
                 'mapped' => false,
                 'required' => false])
            ->add('provincia')
        //    ->add('isActive')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}

//user password encoder  -> para encriptas las passwords de los registros