<?php

namespace App\Repository;

use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Usuario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usuario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usuario[]    findAll()
 * @method Usuario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsuarioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuario::class);
    }

    public function findUsuario(string $search)
    {
        $qb = $this->createQueryBuilder('usuario');

        $qb->where(
            $qb->expr()->orX(
                $qb->expr()->like('usuario.username', ':search'),
                $qb->expr()->like('usuario.correo', ':search')
            )
        );
//        $qb->where($qb->expr()->like('contacto.nombre', ':search'));
//        $qb->orWhere($qb->expr()->like('contacto.telefono', ':search'));
        $qb->setParameter('search', '%' . $search . '%');

        return $qb->getQuery()->execute();
    }

    public function getUsuariosFiltrados(
        string $order, int $idUsuario, string $nombre=null, string $correo=null)
    {
        $qb = $this ->createQueryBuilder('usuario');

        $qb->where($qb->expr()->eq('usuario.role', ':role'))
            ->setParameter('usuario', $idUsuario);

        if (isset($username))
        {
            $qb->andWhere($qb->expr()->like('usuario.username', ':username'))
                ->setParameter('username', '%'.$username.'%');
        }

        if (isset($telefono))
        {
            $qb->andWhere($qb->expr()->like('usuario.correo', ':correo'))
                ->setParameter('correo', '%'.$telefono.'%');
        }

        $qb->orderBy('usuario.'.$order, 'ASC');

        return $qb->getQuery()->getResult();
    }


    // /**
    //  * @return Usuario[] Returns an array of Usuario objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Usuario
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
