<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getPostFiltrados(
        string $order, int $idPost, string $titulo=null, string $usuario=null)
    {
        $qb = $this ->createQueryBuilder('post');

        $qb->where($qb->expr()->eq('post.usuario', ':usuario'))
            ->setParameter('post', $idPost);

        if (isset($titulo))
        {
            $qb->andWhere($qb->expr()->like('post.titulo', ':titulo'))
                ->setParameter('titulo', '%'.$titulo.'%');
        }

        if (isset($usuario))
        {
            $qb->andWhere($qb->expr()->like('post.usuario', ':usuario'))
                ->setParameter('usuario', '%'.$usuario.'%');
        }

        $qb->orderBy('post.'.$order, 'ASC');

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
