<?php


namespace App\BLL;


use App\Entity\Usuario;
use App\Helper\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UsuarioBLL
{
    private $entityManager;
    private $uploader;

    public function __construct(
        EntityManagerInterface $entityManager,
        FileUploader $uploader
    )
    {
        $this->entityManager = $entityManager;
        $this->uploader = $uploader;
    }

    public function eliminar(Usuario $usuario)
    {
        $this->entityManager->remove($usuario);
        $this->entityManager->flush();
    }

    public function guarda(Usuario $usuario, UploadedFile $imgFoto=null)
    {
        if (!is_null($imgFoto))
        {
            $fileName = $this->uploader->upload($imgFoto);

            $usuario->setAvatar($fileName);
        }

        $this->entityManager->persist($usuario);
        $this->entityManager->flush();
    }

}