<?php


namespace App\BLL;


use Doctrine\ORM\EntityManagerInterface;

abstract class BaseBLL
{
    /** @var  EntityManagerInterface $entityManager*/
    protected $entityManager;

    abstract public function toArray($entity) : array;

    /**
     * @required
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function entitiesToArray(array $entities)
    {
        if (is_null($entities))
            return null;

        $arr = [];
        foreach ($entities as $entity)
            $arr[] = $this->toArray($entity);

        return $arr;
    }
}