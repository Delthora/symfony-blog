<?php

namespace App\Controller\REST\v1;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthRestController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction() {
        // The security layer will intercept this request
        return new Response('', Response::HTTP_UNAUTHORIZED);
    }
}