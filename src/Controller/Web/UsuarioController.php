<?php

namespace App\Controller\Web;

use App\BLL\UsuarioBLL;
use App\Entity\Usuario;
use App\Form\UsuarioType;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsuarioController extends AbstractController
{
    private function formContacto(Request $request, UsuarioBLL $usuarioBLL, Usuario $usuario)
    {
        if (!is_null($usuario->getId()))
        {
            $options = [
                'action' => $this->generateUrl("blog_usuario_editar", [ 'id' => $usuario->getId()])
            ];
            $template = 'usuario/editar.html.twig';
            $params = [
                'contacto' => $usuario
            ];
        }
        else
        {
            $options = [];
            $template = 'usuario/listar.html.twig';
            $params = [
                'usuario' => $this->getDoctrine()->getRepository(Usuario::class)->findAll()
            ];
        }

        $form = $this->createForm(UsuarioType::class, $usuario, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $imgFoto = $form->get('AaatarFile')->getData();
            if ($imgFoto instanceof UploadedFile)
            {
                $fileName = md5(uniqid()).'.' . $imgFoto->guessExtension();

                $imgFoto->move(
                    $this->getParameter('fotos_directory'),
                    $fileName
                );

                $usuario->setAvatar($fileName);
            }

            $em->persist($usuario);
            $em->flush();

            $this->addFlash(
                'notice',
                'El contacto se ha guardado correctamente!'
            );

            return $this->redirectToRoute('blog_pages_inicio');
        }

        $params['form'] = $form->createView();

        return $this->render(
            $template,
            $params
        );
    }

    /**
     * @Route(
     *     "/usuarios",
     *     name="blog_usuario_listar",
     *     methods={"GET", "POST"}
     * )
     */
    public function listar(Request $request, UsuarioBLL $usuarioBLL)
    {
        $usuario = new Usuario();
        $template = 'usuario/edit.html.twig';

        return $this->formContacto($request, $usuarioBLL, $usuario);
    }

    /**
     * @Route(
     *     "/usuario/{id}",
     *     name="blog_usuario_editar",
     *     methods={"GET", "POST"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function editar(Request $request, UsuarioBLL $usuarioBLL, Usuario $usuario)
    {
        return $this->formContacto($request, $usuarioBLL, $usuario);
    }

    /**
     * @Route(
     *     "/usuarios/{id}/delete",
     *     name="blog_usuario_eliminar",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function eliminar(Request $request, UsuarioBLL $usuarioBLL, Usuario $usuario)
    {
        $id = $usuario->getId();

        $usuarioBLL->eliminar($usuario);

        $this->addFlash(
            'notice',
            "El contacto $id se ha eliminado correctamente!"
        );

        return $this->redirectToRoute('blog_pages_inicio');
    }

  /*  /**
     * @Route(
     *     "/registro",
     *     name="blog_pages_registro",
     *     methods={"POST", "GET"})

    public function registro(Request $request, PasswordEncoderInterface $codeinterface)
    {
        // just setup a fresh $task object (remove the example data)
        $usuario = new Usuario();

        $form = $this->createForm(UsuarioType::class, $usuario);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $usuario = $form->getData();
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($task);
            // $entityManager->flush();

            return $this->redirectToRoute('blog_pages_inicio');
        }

        return $this->render('usuario/registro.html.twig', [
            'form' => $form->createView(),
        ]);

    }*/

    /**
     * @Route("/", name="usuario_index", methods={"GET"})
     */
    public function index(UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('', [
            'usuarios' => $usuarioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="usuario_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $usuario = new Usuario();
        $form = $this->createForm(UsuarioType::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($usuario);
            $entityManager->flush();

            return $this->redirectToRoute('blog_pages_inicio');
        }

        return $this->render('usuario/new.html.twig', [
            'usuario' => $usuario,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="usuario_show", methods={"GET"})
     */
    public function show(Usuario $usuario): Response
    {
        return $this->render('', [
            'usuario' => $usuario,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="usuario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Usuario $usuario): Response
    {
        $form = $this->createForm(UsuarioType::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('usuario_index');
        }

        return $this->render('usuario/edit.html.twig', [
            'usuario' => $usuario,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="usuario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Usuario $usuario): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usuario->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($usuario);
                $entityManager->flush();
        }

        return $this->redirectToRoute('blog_pages_inicio');
    }
}
