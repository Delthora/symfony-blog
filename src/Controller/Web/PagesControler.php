<?php


namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PagesControler extends AbstractController
{
    /**
     * @Route(
     *     "/",
     *     name="blog_pages_inicio",
     *     methods={"GET"})
     */
    public function inicio()
    {
        return $this->render(
            'pages/inicio.html.twig'
        );
    }

}